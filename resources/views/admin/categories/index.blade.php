@extends('admin.layouts.main_app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Categories</h1>
        </div>
        <div class="row">
            <a href="{{route('categories.create')}}">Create</a>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <th>Наименование</th>
                    <th>Описание</th>
                    <th>Публикация</th>
                    <th>Действие</th>
                </thead>
                <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td>{{$category->title}}</td>
                            <td>{{$category->description}}</td>
                            <td>{{$category->published}}</td>
                            <td>
                                <a href="{{route('categories.edit', $category)}}">Edit</a>
                                <form action="{{route('categories.destroy', $category)}}" method="post">
                                    <input type="hidden" name="_method" value="delete">
                                    {{ csrf_field() }}
                                    <button type="submit">delete</button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="text-center"><h2>Nothing</h2></td>
                        </tr>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">
                            <ul class="pagination">
                                {{$categories->links()}}
                            </ul>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection