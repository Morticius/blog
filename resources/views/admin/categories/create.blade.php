@extends('admin.layouts.main_app')

@section('content')
    <div class="container">
        <form class="form-row" action="{{route('categories.store')}}" method="post">
            {{ csrf_field() }}

            @include('admin.categories.partials.form')
        </form>
    </div>
@endsection