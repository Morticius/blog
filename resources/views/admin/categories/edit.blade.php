@extends('admin.layouts.main_app')

@section('content')
    <div class="container">
        <form class="form-row" action="{{route('categories.update', $category)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.categories.partials.form')
        </form>
    </div>
@endsection