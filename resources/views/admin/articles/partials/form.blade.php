<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($article->id))
        <option value="0" @if ($article->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($article->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="">Название</label>
<input type="text" class="form-control" name="title" placeholder="Название статьи" value="{{{ isset($article->title) ? $article->title : '' }}}" required>

<input type="hidden" name="heading" required>
<input type="hidden" name="heading_json" required>
<label for="">Короткое описание</label>
<editor content="heading" delta="heading_json" default="{{{ isset($article->heading_json) ? $article->heading_json : '{}' }}}"></editor>

<input type="hidden" name="content" required>
<input type="hidden" name="content_json" required>
<label for="">Статья</label>
<editor content="content" delta="content_json" default="{{{ isset($article->content_json) ? $article->content_json : '{}' }}}"></editor>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация" value="{{{ isset($article->slug) ? $article->slug : '' }}}" readonly="">

<label for="">Категория</label>
<select class="form-control" name="category_id">
    @if (isset($article->id))
        @foreach($categories as $category)
            @if ($article->category->id == $category->id)
                <option value="{{$category->id}}" selected="">{{$category->title}}</option>
            @else
                <option value="{{$category->id}}">{{$category->title}}</option>
            @endif
        @endforeach
    @else
        @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->title}}</option>
        @endforeach
    @endif
</select>

<label for="">Теги</label>
<select class="form-control" name="tags[]" multiple>
    @if (isset($article->id))
        @foreach($tags as $simpleTag)
            @foreach($article->tags as $tiedTag)
                    @if ($simpleTag->id == $tiedTag->id)
                        <option value="{{$simpleTag->id}}" selected="">{{$simpleTag->name}}</option>
                    @else
                        <option value="{{$simpleTag->id}}">{{$simpleTag->name}}</option>
                    @endif
            @endforeach
        @endforeach
    @else
        @foreach($tags as $simpleTag)
            <option value="{{$simpleTag->id}}">{{$simpleTag->name}}</option>
        @endforeach
    @endif
</select>

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">