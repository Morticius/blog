@extends('admin.layouts.app')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/default.min.css" rel="stylesheet">
@endpush

@push('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js"></script>
@endpush

@section('content')
    <div class="container">
        @forelse($articles as $article)
            <article class="card">
                <header class="card-header">
                    <p>{{$article->author->name}} {{$article->published_at}}</p>
                    <h2><a href="{{route('articles.show', $article)}}">{{$article->title}}</a></h2>
                    <p>
                        @foreach($article->categories as $category)
                            <span>{{$category->title}} @if (!$loop->last),@endif </span>
                        @endforeach
                    </p>
                </header>
                <main class="card-body">
                    {!! \Illuminate\Support\Str::words($article->content, 25, '...')  !!}
                </main>
                <footer class="card-footer">
                    @forelse($article->tags as $tag)
                        <span>{{$tag->title}}, </span>
                    @empty
                        <span>Nothing</span>
                    @endforelse
                </footer>
            </article>
        @empty
            <h2>Nothing</h2>
        @endforelse
        <ul class="pagination">
            {{$articles->links()}}
        </ul>
    </div>
@endsection