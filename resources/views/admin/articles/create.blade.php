@extends('admin.layouts.app')

@push('styles')
    <link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/default.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.snow.css" rel="stylesheet">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/chosen.jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-tagsinput.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/create-article.js') }}"></script>
@endpush

@section('content')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Добавить статью</h3>
            </div>
        </div>
        <form id="create" method="POST" action="{{route('articles.store')}}">
            @csrf
            <div class="row">
                <div class="col-lg-9 col-md-12">
                    <div class="card card-small mb-3">
                        <div class="card-body">
                            <div class="add-new-post">
                                <select name="categories[]" data-placeholder="Категории" class="chosen-select form-control form-control-lg mb-3" multiple>
                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                                <input name="title" class="form-control form-control-lg mb-3" type="text" placeholder="Заголовок">
                                <input name="content" type="hidden">
                                <div id="editor-container" class="add-new-post__editor mb-1"></div>
                                <input name="tags" class="form-control form-control-lg mb-3" type="text" data-role="tagsinput" placeholder="Теги">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12">
                    <div class='card card-small mb-3'>
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Действия</h6>
                        </div>
                        <div class='card-body p-0'>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item p-3">
                                    <div class="custom-control custom-radio mb-1">
                                        <input
                                                name="published"
                                                type="radio"
                                                class="custom-control-input"
                                                id="published"
                                                value="1"
                                        >
                                        <label class="custom-control-label" for="published">Опубликовано</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-1">
                                        <input
                                                name="published"
                                                type="radio"
                                                class="custom-control-input"
                                                id="draft"
                                                value="0"
                                        >
                                        <label class="custom-control-label" for="draft">Черновик</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Рубрики</h6>
                        </div>
                        <div class='card-body p-0'>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item px-3 pb-2">
                                    @foreach($rubrics as $rubric)
                                        <div class="custom-control custom-checkbox mb-1">
                                            <input name="rubrics[]" type="checkbox" value="{{$rubric->id}}" class="rubrics custom-control-input" id="category{{$rubric->id}}">
                                            <label class="custom-control-label" for="category{{$rubric->id}}">{{$rubric->title}}</label>
                                        </div>
                                    @endforeach
                                </li>
                                <li class="list-group-item d-flex px-3 justify-content-center">
                                    <button type="submit" class="btn btn-sm btn-outline-accent"><i class="material-icons">save</i> Сохранить</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection