@extends('admin.layouts.app')

@section('content')
    <div class="container">
        @if (isset($errors) && count($errors))

            There were {{count($errors->all())}} Error(s)
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }} </li>
                @endforeach
            </ul>

        @endif
        @include('admin.comments.create')
    </div>
@endsection