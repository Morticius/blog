<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="{{ asset('images/shards-dashboards-logo.svg') }}" alt="Shards Dashboard">
                    <span class="d-none d-md-inline ml-1">Shards Dashboard</span>
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>
    <div class="nav-wrapper">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="{{route('admin.index')}}">
                    <i class="material-icons">dashboard</i>
                    <span>Главная</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{route('articles.index')}}">
                    <i class="material-icons">view_module</i>
                    <span>Статьи</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{route('articles.create')}}">
                    <i class="material-icons">note_add</i>
                    <span>Добавить статью</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{route('categories.index')}}">
                    <i class="material-icons">vertical_split</i>
                    <span>Категории</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{route('tags.index')}}">
                    <i class="material-icons">note_add</i>
                    <span>Теги</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="user-profile-lite.html">
                    <i class="material-icons">person</i>
                    <span>User Profile</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="errors.html">
                    <i class="material-icons">error</i>
                    <span>Errors</span>
                </a>
            </li>
        </ul>
    </div>
</aside>