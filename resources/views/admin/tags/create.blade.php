@extends('admin.layouts.main_app')

@section('content')
    <div class="container">
        <form class="form-row" action="{{route('tags.store')}}" method="post">
            {{ csrf_field() }}

            @include('admin.tags.partials.form')
        </form>
    </div>
@endsection