@extends('admin.layouts.main_app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Tags</h1>
        </div>
        <div class="row">
            <a href="{{route('tags.create')}}">Create</a>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <th>Наименование</th>
                <th>Публикация</th>
                <th>Действие</th>
                </thead>
                <tbody>
                @forelse($tags as $tag)
                    <tr>
                        <td>{{$tag->name}}</td>
                        <td>{{$tag->published}}</td>
                        <td>
                            <a href="{{route('tags.edit', $tag)}}">Edit</a>
                            <form action="{{route('tags.destroy', $tag)}}" method="post">
                                <input type="hidden" name="_method" value="delete">
                                {{ csrf_field() }}
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center"><h2>Nothing</h2></td>
                    </tr>
                @endforelse
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="4">
                        <ul class="pagination">
                            {{$tags->links()}}
                        </ul>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection