@extends('admin.layouts.main_app')

@section('content')
    <div class="container">
        <form class="form-row" action="{{route('tags.update', $tag)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            @include('admin.tags.partials.form')
        </form>
    </div>
@endsection