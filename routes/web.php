<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {

    Route::group(['namespace' => 'Admin'], function () {
        Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    });

    Route::group(['namespace' => 'Core'], function () {
        Route::resource('categories', 'CategoryController');
        Route::resource('tags', 'TagController');
        Route::resource('articles', 'ArticleController');

        Route::post('/comments/{article}', [
            'as' => 'comments.store',
            'uses' => 'CommentController@store'
        ]);
        Route::resource('comments', 'CommentController')->except([
            'store'
        ]);

        Route::get('/vote/{vote}/article/{article}', 'VoteController@article');
        Route::get('/vote/{vote}/comment/{comment}', 'VoteController@comment');
    });

});