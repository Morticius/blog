<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = ['javascript', 'c++', 'go', 'php', 'laravel', 'css'];

        foreach ($tags as $tag) {
            \App\Models\Tag::create(['title' => $tag]);
        }
    }
}
