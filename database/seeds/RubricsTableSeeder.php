<?php

use Illuminate\Database\Seeder;

class RubricsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rubrics = [
            'Руководство' => 'tutorial',
            'Обзор' => 'overview',
            'Советы' => 'tips',
            'Интервью' => 'interview',
            'Новости' => 'news',
            'Уроки' => 'lessons'
        ];

        foreach ($rubrics as $rubric => $slug) {
            \App\Models\Rubric::create([
                'title' => $rubric,
                'slug' => $slug,
                'published' => true
            ]);
        }
    }
}