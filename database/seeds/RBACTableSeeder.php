<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RBACTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['admin', 'writer', 'reader', 'guest'];

        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }

        $user = \App\User::where('email', 'rostislav.khantil@gmail.com')->first();
        $user->assignRole($roles);
    }
}
