<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['title'];

    public static function syncTags($tags)
    {
        $ids = [];
        $tags = explode(',', $tags);

        foreach ($tags as $tagTitle) {
            $tag = Tag::whereTitle($tagTitle)->first();

            if(is_null($tag)) {
                $tag = Tag::create(['title' => $tagTitle]);
            }
            array_push($ids, $tag->id);
        }

        return $ids;
    }
}
