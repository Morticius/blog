<?php

namespace App\Http\Controllers\Core;

use App\Models\Article;
use App\Models\Comment;
use App\Http\Controllers\Controller;

class VoteController extends Controller
{
    public function article($vote, Article $article)
    {
        $article->votes()->create(['vote' => $vote]);
    }

    public function comment($vote, Comment $comment)
    {
        $comment->votes()->create(['vote' => $vote]);
    }
}
