<?php

namespace App\Http\Controllers\Core;

use App\Models\{Article, Category, Rubric, Tag, Vote};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    protected $article;
    protected $tag;
    protected $category;
    protected $rubrics;

    public function __construct(Article $article, Tag $tag, Category $category, Rubric $rubrics)
    {
        $this->article = $article;
        $this->category = $category;
        $this->tag = $tag;
        $this->rubrics = $rubrics;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.articles.index', [
            'articles' => $this->article->getWithAllData()->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create', [
            'article'   => [],
            'categories' => $this->category->get(),
            'rubrics' => $this->rubrics->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = $request->user()->articles()->create($request->all());
        $article->categories()->attach($request->categories);
        $article->rubrics()->attach($request->rubrics);
        $article->tags()->attach(Tag::syncTags($request->tags));
        return redirect()->route('articles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('admin.articles.show', [
            'article' => $article->getWithAllData()->where('id', $article->id)->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('admin.articles.edit', [
            'article'   => $article->getWithAllData()->where('id', $article->id)->first(),
            'categories' => $this->category->get(),
            'rubrics' => $this->rubrics->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Article $article)
    {
        $article->update($request->all());
        $article->categories()->sync($request->categories);
        $article->rubrics()->sync($request->rubrics);
        $article->tags()->sync(Tag::syncTags($request->tags));

        return redirect()->route('articles.show', $article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('articles.index');
    }
}
