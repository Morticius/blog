'use strict';

(function ($) {
    $(document).ready(function () {
        $(".chosen-select").chosen({
            inherit_select_classes: true
        });

        var toolbarOptions = [
            [{ 'header': [1, 2, 3, 4, 5, false] }],
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            [{ 'header': 1 }, { 'header': 2 }],
            ['image', 'code-block'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],
        ];

        var quill = new Quill('#editor-container', {
            modules: {
                syntax: true,
                toolbar: toolbarOptions
            },
            placeholder: 'Words can be like x-rays if you use them properly...',
            theme: 'snow'
        });

        quill.clipboard.dangerouslyPasteHTML($("input[name='content']").val());

        quill.on('text-change', function () {
            $("input[name='content']").val(quill.root.innerHTML);
        });
    });
})(jQuery);